-- Your SQL goes here

CREATE TABLE "users" (
  "id" BLOB NOT NULL PRIMARY KEY,
  "name" VARCHAR(20) NOT NULL,
  "hashpw" VARCHAR(60) NOT NULL,
  "email" VARCHAR(255) NOT NULL
);

CREATE TABLE "channels" (
  "id" BLOB NOT NULL PRIMARY KEY,
  "name" VARCHAR(20) NOT NULL,
  "owner_id" BLOB NOT NULL
);

CREATE TABLE "channel_members" (
  "channel_id" BLOB NOT NULL,
  "user_id" BLOB NOT NULL,
  PRIMARY KEY ("channel_id", "user_id")
);

CREATE TABLE "messages" (
  "id" BLOB NOT NULL PRIMARY KEY,
  "content" TEXT NOT NULL,
  "author_id" BLOB NOT NULL,
  "channel_id" BLOB NOT NULL,
  "time" DATETIME NOT NULL
);