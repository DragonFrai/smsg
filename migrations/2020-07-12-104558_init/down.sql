-- This file should undo anything in `up.sql`

DROP TABLE users;
DROP TABLE channels;
DROP TABLE messages;
DROP TABLE channel_members;