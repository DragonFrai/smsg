use crate::models::dao::*;
use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use r2d2::{Pool, PooledConnection};
use std::fmt::{Display, Formatter};

use crate::models::Id;
use crate::schema::channel_members::dsl as cm;
use crate::schema::channels::dsl as c;
use crate::schema::messages::dsl as m;
use crate::schema::users::dsl as u;

pub type DbConnection = diesel::SqliteConnection;
pub type DbManager = ConnectionManager<DbConnection>;
pub type DbPool = Pool<DbManager>;

// -----
// Error
// -----

#[derive(Debug)]
pub enum Error {
    Connection(r2d2::Error),
    Query(diesel::result::Error),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Connection(_) => f.write_str("Connection error."),
            Self::Query(_) => f.write_str("Query error."),
        }
    }
}

impl From<r2d2::Error> for Error {
    fn from(e: r2d2::Error) -> Self {
        Self::Connection(e)
    }
}

impl From<diesel::result::Error> for Error {
    fn from(e: diesel::result::Error) -> Self {
        Self::Query(e)
    }
}

pub type Result<T> = std::result::Result<T, Error>;

// ---------
// DbService
// ---------

#[derive(Clone)]
pub struct DbService {
    pool: DbPool,
}

impl DbService {
    pub fn new(pool: DbPool) -> Self {
        Self { pool }
    }

    fn get(&self) -> std::result::Result<PooledConnection<DbManager>, r2d2::Error> {
        self.pool.get()
    }

    pub fn add_user(&self, user: &UserDao) -> Result<()> {
        let conn = &self.get()?;
        diesel::insert_into(u::users).values(user).execute(conn)?;
        Ok(())
    }

    pub fn user_by_id(&self, user_id: &Id) -> Result<Option<UserDao>> {
        let conn = &self.get()?;
        let res: QueryResult<UserDao> = u::users.filter(u::id.eq(user_id)).first(conn);
        //res.optional().map_err(|e| e.into())
        Ok(res.optional()?)
    }

    pub fn user_by_name(&self, name: &str) -> Result<Option<UserDao>> {
        let conn = &self.get()?;
        let res: QueryResult<UserDao> = u::users.filter(u::name.eq(name)).first(conn);
        //res.optional().map_err(|e| e.into())
        Ok(res.optional()?)
    }

    pub fn user_is_registered(&self, name: &str) -> Result<bool> {
        Ok(self.user_by_name(name)?.is_some())
    }

    pub fn add_channel(&self, channel: &ChannelDao) -> Result<()> {
        let conn = &self.get()?;
        diesel::insert_into(c::channels)
            .values(channel)
            .execute(conn)?;
        Ok(())
    }

    pub fn user_channels(&self, user_id: &Id) -> Result<Vec<ChannelDao>> {
        let conn = &self.get()?;
        Ok(c::channels.filter(c::owner_id.eq(user_id)).load(conn)?)
    }

    pub fn channels_with_member(&self, user_id: &Id) -> Result<Vec<ChannelDao>> {
        let conn = &self.get()?;
        // let result = ch::channels
        //     .inner_join(cu::channel_user.on(ch::id.eq(cu::channel_id)))
        //     .filter(cu::user_id.eq(user_id.0))
        //     .load::<(ChannelDao, ChannelUserDao)>(&db)?;

        let channels_cm: Vec<(ChannelDao, ChannelMemberDao)> = c::channels
            .inner_join(cm::channel_members.on(c::id.eq(cm::channel_id)))
            .filter(cm::user_id.eq(user_id))
            .load(conn)?;

        let channels = channels_cm.into_iter().map(|(c, _)| c).collect();

        Ok(channels)
    }

    pub fn get_messages(&self, channel_id: &Id) -> Result<Vec<MessageDao>> {
        let conn = &self.get()?;
        let msgs = m::messages
            .filter(m::channel_id.eq(channel_id))
            .load(conn)?;
        Ok(msgs)
    }

    pub fn add_member(&self, channel_id: &Id, user_id: &Id) -> Result<()> {
        let conn = &self.get()?;
        let record = ChannelMemberDao {
            channel_id: channel_id.clone(),
            user_id: user_id.clone(),
        };
        diesel::insert_into(cm::channel_members)
            .values(record)
            .execute(conn)?;
        Ok(())
    }

    pub fn add_message(&self, msg: &MessageDao) -> Result<()> {
        let conn = &self.get()?;
        diesel::insert_into(m::messages).values(msg).execute(conn)?;
        Ok(())
    }
}
