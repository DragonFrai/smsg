#[macro_use]
extern crate diesel;

pub mod actors;
pub mod api;
pub mod auth;
pub mod db;
pub mod models;
pub mod schema;

use crate::db::{DbPool, DbService};
use crate::models::dto::UserDto;
use crate::models::Id;
use actix_cors::Cors;
use actix_web::middleware::Logger;
use actix_web::web;
use actix_web::{App, HttpServer};
use bcrypt::DEFAULT_COST;
use diesel::r2d2::ConnectionManager;
use diesel::{RunQueryDsl, SqliteConnection};
use jsonwebtoken::{DecodingKey, EncodingKey};
use log::{debug, error, info, log, warn};
use models::dao::*;
use models::domain::*;
use r2d2::{ManageConnection, Pool};
use std::convert::TryInto;
use uuid::Uuid;

pub fn jwt_decoding_key() -> DecodingKey<'static> {
    DecodingKey::from_base64_secret(
        &std::env::var("JWT_DECODE_KEY").expect("JWT_DECODE_KEY must be set."),
    )
    .expect("Illegal decode key.")
}

pub fn jwt_encoding_key() -> EncodingKey {
    EncodingKey::from_base64_secret(
        &std::env::var("JWT_ENCODE_KEY").expect("JWT_ENCODE_KEY must be set."),
    )
    .expect("Illegal encode key.")
}

pub fn connect_to_db() -> DbPool {
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set.");
    let conn_man: ConnectionManager<SqliteConnection> = ConnectionManager::new(db_url);
    Pool::new(conn_man).unwrap()
}

fn bind_url() -> String {
    let bind = std::env::var("BIND").unwrap_or("127.0.0.1:8080".to_string());
    bind
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().unwrap();
    env_logger::init();

    let bind = bind_url();

    info!("Starting server at: {}", &bind);

    let pool = connect_to_db();
    let jwt_encoding_key = jwt_encoding_key();
    let jwt_decoding_key = jwt_decoding_key();

    let db_service = DbService::new(pool.clone());

    // Start HTTP server
    HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())
            .wrap(Logger::default())
            .wrap(Cors::default())
            .data(pool.clone())
            .data(db_service.clone())
            .data(jwt_encoding_key.clone())
            .data(jwt_decoding_key.clone())
            .configure(crate::api::v0::config_api_v0)
    })
    .bind(&bind)?
    .run()
    .await
}
