table! {
    channel_members (channel_id, user_id) {
        channel_id -> Binary,
        user_id -> Binary,
    }
}

table! {
    channels (id) {
        id -> Binary,
        name -> Text,
        owner_id -> Binary,
    }
}

table! {
    messages (id) {
        id -> Binary,
        content -> Text,
        author_id -> Binary,
        channel_id -> Binary,
        time -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Binary,
        name -> Text,
        hashpw -> Text,
        email -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    channel_members,
    channels,
    messages,
    users,
);
