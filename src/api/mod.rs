pub mod v0;

use actix_web::http::StatusCode;
use actix_web::{Responder, ResponseError};
use std::fmt::{Display, Formatter};

use crate::db::Error as DbError;
use bcrypt::BcryptError;

use actix_web::error::Error as AError;
use actix_web::error::*;
use actix_web::HttpResponse;
use std::borrow::Borrow;
use std::fmt::Debug;

#[derive(Debug)]
pub struct Error(pub AError);

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

impl ResponseError for Error {
    #[inline]
    fn status_code(&self) -> StatusCode {
        self.0.as_response_error().status_code()
    }

    #[inline]
    fn error_response(&self) -> HttpResponse {
        self.0.as_response_error().error_response()
    }
}

impl From<AError> for Error {
    fn from(e: AError) -> Self {
        Self(e)
    }
}

impl From<DbError> for Error {
    fn from(e: DbError) -> Self {
        ErrorInternalServerError(e).into()
    }
}

impl From<BcryptError> for Error {
    fn from(e: BcryptError) -> Self {
        ErrorInternalServerError(e).into()
    }
}

impl From<jsonwebtoken::errors::Error> for Error {
    fn from(e: jsonwebtoken::errors::Error) -> Self {
        ErrorInternalServerError(e).into()
    }
}

pub type Result<T> = std::result::Result<T, Error>;

// -------------------
// Helper constructors
// -------------------

#[allow(non_snake_case)]
pub fn BadRequest<T>(err: T) -> Error
where
    T: Display + Debug + 'static,
{
    ErrorBadRequest(err).into()
}

#[allow(non_snake_case)]
pub fn InternalServerError<T>(err: T) -> Error
where
    T: Display + Debug + 'static,
{
    ErrorInternalServerError(err).into()
}
