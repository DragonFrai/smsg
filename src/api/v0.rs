use crate::api::BadRequest;
use crate::auth::{encode_token, UserToken};
use crate::db::DbService;
use crate::models::dao::*;
use crate::models::domain::*;
use crate::models::dto::*;
use crate::models::{ChannelId, Id, UserId};
use actix_web::web;
use actix_web::web::{Data, Json};
use actix_web::{post, HttpRequest, HttpResponse, Responder};
use diesel::prelude::*;
use jsonwebtoken::EncodingKey;
use lazy_static::*;

use crate::api::Result;
use actix_web::error::ErrorBadRequest;
use chrono::Utc;

pub fn config_api_v0(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/api/v0")
            .wrap(crate::auth::Identity::new())
            .service(ping)
            .service(signup)
            .service(signin)
            .service(create_channel)
            // .service(my_channels)
            .service(get_messages)
            .service(channels)
            .service(join_to_channel)
            .service(send_message),
    );
}

lazy_static! {
    pub static ref ALLOWED_NAME_CHARS: Vec<char> =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"
            .chars()
            .collect();
}

// ---------
// Util Func
// ---------

fn check_user_name(name: &str) -> Result<()> {
    let br = |s: &str| Err(BadRequest(s.to_string()));

    if name.len() < 3 {
        return br("Имя пользователя слишком короткое. Оно должно быть не менее 3 символов.");
    } else if name.len() > 20 {
        return br("Имя пользователя слишком длинное. Оно должно быть не более 20 символов.");
    }

    // Проверка символов имени
    for ch in name.chars() {
        if !ALLOWED_NAME_CHARS.contains(&ch) {
            return br("Имя пользователя должно содержать только латинский буквы, цифры и знак подчеркивания '_'.");
        }
    }

    Ok(())
}

fn check_user_password(password: &str) -> Result<()> {
    let br = |s: &str| Err(BadRequest(s.to_string()));
    if password.len() < 1 {
        return br("Пароль не может быть пустым.");
    }
    Ok(())
}

// ------------
// API Handlers
// ------------

#[post("/ping")]
pub async fn ping(_: HttpRequest) -> impl Responder {
    HttpResponse::Ok().body("pong!")
}

#[post("signup")]
pub async fn signup(db: Data<DbService>, sud: Json<SignUpDto>) -> Result<impl Responder> {
    use crate::schema::users::dsl as u;

    let db = db.get_ref();

    let SignUpDto { name, password } = sud.0;

    check_user_name(&name)?;
    check_user_password(&password)?;

    if db.user_is_registered(&name)? {
        return Err(BadRequest(
            "Пользователь с таким именем уже зарегистрирован.".to_string(),
        ));
    }

    let hashpw = bcrypt::hash(&password, bcrypt::DEFAULT_COST)?;

    let user = User {
        id: UserId(Id::new()),
        name,
        hashpw,
        email: "".to_string(),
    };

    db.add_user(&user.into())?;

    Ok(HttpResponse::Ok().finish())
}

#[post("signin")]
pub async fn signin(
    db: Data<DbService>,
    sid: Json<SignInDto>,
    key: Data<EncodingKey>,
) -> Result<impl Responder> {
    let SignInDto { name, password } = sid.0;

    const ILLEGAL_LOGIN_OR_PASSWORD: &str = "Неверный логин или пароль.";

    let user: User = db
        .get_ref()
        .user_by_name(&name)?
        .ok_or_else(|| BadRequest(ILLEGAL_LOGIN_OR_PASSWORD.to_string()))?
        .into();

    match bcrypt::verify(&password, &user.hashpw)? {
        true => {
            let user_token = UserToken {
                id: user.id.clone(),
                exp: i64::MAX,
            };

            let token = encode_token(&user_token, &jsonwebtoken::Header::default(), key.get_ref())?;

            let body = SignInResponseDto {
                token,
                me: user.into(),
            };

            Ok(HttpResponse::Ok().json(body))
        }

        false => Err(ErrorBadRequest("Неверный логин или пароль.".to_string()).into()),
    }
}

#[post("get_messages")]
pub async fn get_messages(db: Data<DbService>, ci: Json<Id>) -> Result<impl Responder> {
    let msgs = db.get_ref().get_messages(&ci)?;
    let mut msgs_dtos = Vec::new();

    for m in msgs {
        let user = db.get_ref().user_by_id(&m.author_id)?.unwrap();
        let MessageDao {
            id,
            content,
            author_id,
            channel_id,
            time,
        } = m;
        let dto = MessageDto {
            id,
            content,
            author: User::from(user).into(),
            time,
        };

        msgs_dtos.push(dto);
    }
    Ok(Json(msgs_dtos))
}

/// Get Me channels
#[post("channels")]
pub async fn channels(db: Data<DbService>, me: UserToken) -> Result<impl Responder> {
    let channels = db.get_ref().channels_with_member(&me.id.0)?;
    let channels: Vec<ChannelDto> = channels
        .into_iter()
        .map(|c| Channel::from(c).into())
        .collect();

    Ok(Json(channels))
}

#[post("create_channel")]
pub async fn create_channel(
    db: Data<DbService>,
    cc: Json<CreateChannelDto>,
    me: UserToken,
) -> Result<impl Responder> {
    // TODO: Добавить проверку существования канала

    let owner_id = me.id;

    let CreateChannelDto { name } = cc.0;

    let channel = Channel {
        id: ChannelId(Id::new()),
        name,
        owner_id,
    };

    let channel = channel.into();

    db.get_ref().add_channel(&channel)?;
    db.get_ref().add_member(&channel.id, &channel.owner_id)?;

    Ok(HttpResponse::Ok())
}

#[post("join_to_channel")]
pub async fn join_to_channel(
    db: Data<DbService>,
    cc: Json<JoinToChannelDto>,
    me: UserToken,
) -> Result<impl Responder> {
    let user_id = me.id;
    let channel_id = (cc.0).0;

    db.as_ref().add_member(&channel_id.0, &user_id.0)?;

    Ok(HttpResponse::Ok())
}

#[post("send_msg")]
pub async fn send_message(
    db: Data<DbService>,
    smt: Json<SendMessageDto>,
    me: UserToken,
) -> Result<impl Responder> {
    let SendMessageDto {
        content,
        channel_id,
    } = smt.0;
    let msg = MessageDao {
        id: Id::new(),
        author_id: me.id.0.clone(),
        channel_id,
        content,
        time: Utc::now().naive_utc(),
    };
    db.get_ref().add_message(&msg)?;
    Ok(HttpResponse::Ok().finish())
}
