use crate::models::domain::{Channel, Message, User};
use crate::models::{ChannelId, Id, UserId};
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UserDto {
    pub id: UserId,
    pub name: String,
}

impl From<User> for UserDto {
    fn from(u: User) -> Self {
        let User { id, name, .. } = u;
        Self { id, name }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SignUpDto {
    pub name: String,
    pub password: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChannelDto {
    pub id: Id,
    pub name: String,
    pub owner_id: Id,
}

impl From<Channel> for ChannelDto {
    fn from(c: Channel) -> Self {
        let Channel { id, name, owner_id } = c;
        Self {
            id: id.0,
            name,
            owner_id: owner_id.0,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ChannelsDto(pub Vec<ChannelDto>);

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MessageDto {
    pub id: Id,
    pub content: String,
    pub author: UserDto,
    pub time: NaiveDateTime,
}

// impl From<Message> for MessageDto {
//     fn from(m: Message) -> Self {
//         let Message {
//             content,
//             author_id,
//             time,
//             id,
//             ..
//         } = m;
//         Self {
//             id: id.0,
//             content,
//             author_id: author_id.0,
//             time,
//         }
//     }
// }

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SendMessageDto {
    pub content: String,
    pub channel_id: Id,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SignInDto {
    pub name: String,
    pub password: String,
}

// #[derive(Debug, Serialize, Deserialize)]
// #[serde(rename_all = "camelCase")]
// pub struct GetMessagesDto {}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CreateChannelDto {
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JoinToChannelDto(pub ChannelId);

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SignInResponseDto {
    pub token: String,
    pub me: UserDto,
}
