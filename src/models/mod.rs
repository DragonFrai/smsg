pub mod dao;
pub mod domain;
pub mod dto;

use diesel::backend::Backend;
use diesel::deserialize::{FromSql, FromSqlRow};
use diesel::insertable::ColumnInsertValue;
use diesel::insertable::InsertValues;
use diesel::row::Row;
use diesel::serialize::{Output, ToSql};
use diesel::sql_types::Binary;
use diesel::sqlite::Sqlite;
use diesel::Expression;
use diesel::{Insertable, Queryable};
use serde::{Deserialize, Serialize};
use std::io::Write;
use uuid::Uuid;

// --
// Id
// --

#[derive(
    Copy,
    Clone,
    Ord,
    PartialOrd,
    Eq,
    PartialEq,
    Hash,
    Debug,
    Serialize,
    Deserialize,
    FromSqlRow,
    AsExpression,
)]
#[sql_type = "Binary"]
pub struct Id(pub Uuid);

impl Id {
    pub fn new() -> Self {
        Self(Uuid::new_v4())
    }
}

impl From<Uuid> for Id {
    fn from(u: Uuid) -> Self {
        Self(u)
    }
}

impl From<Id> for Uuid {
    fn from(i: Id) -> Self {
        i.0
    }
}

// SqLite Ext

impl FromSql<Binary, Sqlite> for Id {
    fn from_sql(
        bytes: Option<&<Sqlite as Backend>::RawValue>,
    ) -> diesel::deserialize::Result<Self> {
        match bytes {
            Some(v) => {
                let blob = v.read_blob();
                let uuid = Uuid::from_slice(blob).map_err(|e| e.to_string())?;
                Ok(Id(uuid))
            }
            None => Err(format!("Null blob value.").into()),
        }
    }
}

impl ToSql<Binary, Sqlite> for Id {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Sqlite>) -> diesel::serialize::Result {
        let b = self.0.as_bytes() as &[u8];
        ToSql::<Binary, Sqlite>::to_sql(b, out)
    }
}

// Sub Id

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UserId(pub Id);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ChannelId(pub Id);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MessageId(pub Id);
