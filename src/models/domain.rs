use crate::models::{ChannelId, Id, MessageId, UserId};
use chrono::NaiveDateTime;

#[derive(Debug)]
pub struct User {
    pub id: UserId,
    pub email: String,
    pub name: String,
    pub hashpw: String,
}

pub struct Channel {
    pub id: ChannelId,
    pub name: String,
    pub owner_id: UserId,
}

pub struct Message {
    pub id: MessageId,
    pub content: String,
    pub author_id: UserId,
    pub channel_id: ChannelId,
    pub time: NaiveDateTime,
}
