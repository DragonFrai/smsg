use crate::models::domain::{Channel, Message, User};
use crate::models::{domain, ChannelId, Id, MessageId, UserId};
use crate::schema::*;
use chrono::NaiveDateTime;
use diesel::backend::Backend;
use diesel::deserialize::FromSql;
use diesel::serialize::{Output, ToSql};
use diesel::sql_types::Blob;
use diesel::sqlite::Sqlite;
use diesel::{Identifiable, Insertable, Queryable, SqliteConnection};
use std::convert::{From, TryFrom};
use std::io::Write;
use uuid::Uuid;

// pub struct FromDaoError;
// type FromDaoResult<T> = std::result::Result<T, FromDaoError>;

// -------
// UserDao
// -------

#[derive(Debug, Queryable, Insertable, Identifiable)]
#[table_name = "users"]
pub struct UserDao {
    pub id: Id,
    pub name: String,
    pub hashpw: String,
    pub email: String,
}

impl From<User> for UserDao {
    fn from(domain: User) -> Self {
        let User {
            id,
            email,
            name,
            hashpw,
        } = domain;

        Self {
            id: id.0,
            email,
            name,
            hashpw,
        }
    }
}

impl From<UserDao> for User {
    fn from(u: UserDao) -> Self {
        let UserDao {
            id,
            email,
            name,
            hashpw,
        } = u;

        Self {
            id: UserId(id),
            email,
            name,
            hashpw,
        }
    }
}

// ----------
// ChannelDao
// ----------

#[derive(Debug, Queryable, Insertable, Identifiable)]
#[table_name = "channels"]
pub struct ChannelDao {
    pub id: Id,
    pub name: String,
    pub owner_id: Id,
}

impl From<Channel> for ChannelDao {
    fn from(c: Channel) -> Self {
        let Channel {
            id,
            name,
            owner_id: owner,
        } = c;
        Self {
            id: id.0,
            name,
            owner_id: owner.0,
        }
    }
}

impl From<ChannelDao> for Channel {
    fn from(c: ChannelDao) -> Self {
        let ChannelDao {
            id,
            name,
            owner_id: owner,
        } = c;
        Self {
            id: ChannelId(id),
            name,
            owner_id: UserId(owner),
        }
    }
}

// ----------------
// ChannelMemberDao
// ----------------

#[derive(Debug, Queryable, Insertable, Identifiable)]
#[table_name = "channel_members"]
#[primary_key(channel_id, user_id)]
pub struct ChannelMemberDao {
    pub channel_id: Id,
    pub user_id: Id,
}

// ----------
// MessageDao
// ----------

#[derive(Debug, Queryable, Insertable, Identifiable)]
#[table_name = "messages"]
pub struct MessageDao {
    pub id: Id,
    pub content: String,
    pub author_id: Id,
    pub channel_id: Id,
    pub time: NaiveDateTime,
}

impl From<Message> for MessageDao {
    fn from(m: Message) -> Self {
        let Message {
            id,
            author_id,
            content,
            channel_id,
            time,
        } = m;
        Self {
            id: id.0,
            author_id: author_id.0,
            channel_id: channel_id.0,
            content,
            time,
        }
    }
}

impl From<MessageDao> for Message {
    fn from(m: MessageDao) -> Self {
        let MessageDao {
            id,
            author_id,
            content,
            channel_id,
            time,
        } = m;
        Self {
            id: MessageId(id),
            author_id: UserId(author_id),
            channel_id: ChannelId(channel_id),
            content,
            time,
        }
    }
}
